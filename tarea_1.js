//2) ¿cuantos registros arrojó el comando count?
db.grades.count()
//800

//3) encuentra todas las calificaciones del estudiante con el id numero 4
db.grades.find({"student_id":4});
/*
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
*/ 
//4) ¿Cuántos registros hay de tipo exam?
db.grades.find({"type":"exam"}).count();
//hay 200 registros tipo exam
//5) ¿Cuántos registros hay de tipo homework? 
db.grades.find({"type":"homework"}).count();
//hay 400 registros tipo homework
//6) ¿Cuántos registros hay de tipo quiz? 
db.grades.find({"type":"quiz"}).count();
//hay 200 registros tipo quiz
//7) Elimina todas las calificaciones del estudiante con el id numero 3 
db.grades.updateMany({"student_id":3}, {$unset: {score:""}});
db.grades.find({"student_id":3});
/*
{ "_id" : ObjectId("50906d7fa3c412bb040eb583"), "student_id" : 3, "type" : "exam" }
{ "_id" : ObjectId("50906d7fa3c412bb040eb584"), "student_id" : 3, "type" : "quiz" }
{ "_id" : ObjectId("50906d7fa3c412bb040eb585"), "student_id" : 3, "type" : "homework" }
{ "_id" : ObjectId("50906d7fa3c412bb040eb586"), "student_id" : 3, "type" : "homework" }
*/ 
//8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ? 
db.grades.find({type:"homework", "score":75.29561445722392});
/*
{ "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }
es el estudiante 9
 */
//9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100 
db.grades.updateOne({"_id":ObjectId("50906d7fa3c412bb040eb591")}, {$set: {"score":100}});
/*
{ "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }
*/
//10) A qué estudiante pertenece esta calificación.
db.grades.find({"_id":ObjectId("50906d7fa3c412bb040eb591")});
/*
Pertenece al estudiante 6 y es una homework
{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
 */
